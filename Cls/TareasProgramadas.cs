﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace BFGestorTareas
{
    public class TareasProgramadas
    {
        public static string SQL_GTBBDD_CadenaConexion { get; set; }

        public TareasProgramadas()
        {
            SQL_GTBBDD_CadenaConexion = @"Server=192.168.0.102\sqlexpress2017;Database=GestorTareas;User Id=sa;password=1Qaz2wsx@;Trusted_Connection=False;MultipleActiveResultSets=true;";
        }

        public List<TablaTareaProgramada> getTareas()
        {
            string connString = SQL_GTBBDD_CadenaConexion;
            List<TablaTareaProgramada> _tareas = new List<TablaTareaProgramada>();
            //variables to store the query results            

            try
            {
                //sql connection object
                using (SqlConnection conn = new SqlConnection(connString))
                {

                    string query = @" SELECT * from TareasProgramadas  order by Id";
                    SqlCommand cmd = new SqlCommand(query, conn);
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            TablaTareaProgramada tp = new TablaTareaProgramada();
                            DateTime d = new DateTime();
                            tp.Id = dr.GetInt32(0);
                            tp.APP = dr.GetString(1);
                            tp.Nombre = dr.GetString(2);
                            tp.DLL = dr.GetString(3);
                            tp.Namespace = dr.GetString(4);
                            tp.Clase = dr.GetString(5);
                            tp.Metodo = dr.GetString(6);
                            tp.Intervalo = dr.GetInt32(7);
                            tp.Repetir = dr.GetBoolean(8);
                            tp.ForzarInicio = dr.GetBoolean(9);
                            tp.ParametroAPP = dr.IsDBNull(10) == true ? "" : dr.GetString(10);
                            _tareas.Add(tp);





                        }
                    }
                    else
                    {
                        Console.WriteLine("No data found.");
                    }

                    //close data reader
                    dr.Close();

                    //close connection
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                //display error message
                Console.WriteLine("Exception: " + ex.Message);
            }

            return _tareas;
        }


    }

    public class TablaTareaProgramada
    {
        public int Id { get; set; }
        public string APP { get; set; }
        public string Nombre { get; set; }
        public string DLL { get; set; }
        public string Namespace { get; set; }
        public string Clase { get; set; }
        public string Metodo { get; set; }
        public int Intervalo { get; set; }
        public bool Repetir { get; set; }
        public bool ForzarInicio { get; set; }
        public string ParametroAPP { get; set; }

    }
}
