﻿using BFGestorTareas.Model;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
// https://www.c-sharpcorner.com/blogs/download-amazon-reports-using-mws-api

namespace BFGestorTareas

{

    public class GestorTareas
    {
        public delegate void GestorTareaChangeHandler(object sender, GestorInfoEventArgs e);

        public GestorTareaChangeHandler GestorTareaChange;

        Clock theClock;
        LogGestor log;

        DateTime timeClock;
        DateTime inicioGestor;
        TimeSpan tiempoactividad;
        CancellationTokenSource cts;

        List<TareaProgramada> tareasprogramadas = null;
        ConcurrentDictionary<TareaProgramada, Task> listaTareasCreadas = null;
        ConcurrentDictionary<Task, TareaProgramada> listaTareasActivas = null;
        GestorInfoEventArgs gestorInformation = null;

        public GestorTareas()
        {
            inicioGestor = DateTime.Now;
            tareasprogramadas = new List<TareaProgramada>();
            listaTareasCreadas = new ConcurrentDictionary<TareaProgramada, Task>();
            listaTareasActivas = new ConcurrentDictionary<Task, TareaProgramada>();
            log = LogGestor.GetInstance;
            theClock = Clock.GetInstance;
            theClock.SecondChanged += new Clock.SecondChangeHandler(TimeHasChanged);
        }

        public void AddTarea(TareaProgramada tarea)
        {
            object obj = new object();
            string err = string.Empty;

            try
            {
                //lock (obj)
                //{

                // ****************************************************
                // 1 - Poner estas propiedades obligatorias en la clase;
                // 2 - O Añadirlas, no procesarlas y a avisar en el log
                // ****************************************

                //if (tarea.tsintervalo.Ticks == 0)
                //    return;

                if (tarea.tarea == null)
                {
                    err = "error => La tarea no pude ser NULL ";
                    throw new Exception(err);
                }

                if (tarea.descripcion == null)
                {
                    err = $"[Tarea {tarea.tarea.Method.Name}]=> error => No pertenece a nadie, asignar descripción";
                    throw new Exception(err);
                }

                if (tarea.tsintervalo.Ticks == 0 && tarea.repetir)
                {
                    err = $"[Tarea {tarea.tarea.Method.Name}]=> error => Falta intervalo de repetición";
                    throw new Exception(err);
                }

                if (tarea.tsintervalo.Ticks == 0 && !tarea.repetir && !tarea.forzarinicio)
                {
                    err = $"[Tarea {tarea.tarea.Method.Name}]=> error => Falta intervalo de activación";
                    throw new Exception(err);
                }

                var index = tareasprogramadas.IndexOf(tarea);

                //if (!tareasprogramadas.Where(a => a.descripcion == tarea.descripcion && a.tarea.Method == tarea.tarea.Method).Count().Equals(0)) // Casca por los bloqueos
                if (index != -1)
                {
                    err = $"[tarea {tarea.tarea.Method.Name}]=> error => no se incluye porque ya existe ";
                    throw new Exception(err);
                }


                DateTime dt = DateTime.Now;
                tarea.hhInicio = dt;
                tarea.hhCreada = dt;
                tarea.hhFin = dt;
                tarea.IdTarea = 0;
                tareasprogramadas.Add(tarea);
                //}

            }
            catch (Exception ex)
            {
                GrabaLog(ConsoleColor.Red, ex.Message);
                Console.ReadLine();
                //throw new Exception(ex.Message);
            }
            finally
            {
            }
        }

        public void RemoveTarea(TareaProgramada tarea)
        {
            tareasprogramadas.Remove(tarea);
        }

        public void StartGestor()
        {
            theClock.StarClock();
        }

        public void StopGestor()
        {
            theClock.StopClock();
        }

        void CreaTareas()
        {
            try
            {

                //Parallel.ForEach(
                //       tareasprogramadas,
                //        new ParallelOptions() { MaxDegreeOfParallelism = 2 },
                //       (itemtareas) =>
                //       {

                foreach (var itemtareas in tareasprogramadas.OrderBy(o => o.descripcion))
                {
                    if (!listaTareasCreadas.ContainsKey(itemtareas))
                    {
                        var index = tareasprogramadas.IndexOf(itemtareas);
                        tareasprogramadas[index].hhCreada = timeClock;
                        tareasprogramadas[index].hhInicio = tareasprogramadas[index].forzarinicio ? tareasprogramadas[index].hhCreada : timeClock.Add(tareasprogramadas[index].tsintervalo);
                        tareasprogramadas[index].forzarinicio = false;
                        //listaTareasCreadas.GetOrAdd(itemtareas, new Task(new Action(itemtareas.tarea)));
                        //listaTareasCreadas.GetOrAdd(itemtareas, new Task(new Action(itemtareas.tarea), TaskCreationOptions.LongRunning));
                        listaTareasCreadas.GetOrAdd(itemtareas, new Task(new Action(itemtareas.tarea), (TaskCreationOptions.PreferFairness | TaskCreationOptions.DenyChildAttach)));
                        //listaTareasCreadas.GetOrAdd(itemtareas, new Task(new Action(itemtareas.tarea), (TaskCreationOptions.PreferFairness | TaskCreationOptions.AttachedToParent)));
                    }
                }//);

                IniciaTareas();
                FinalizaTareas();

            }
            catch (Exception ex)
            {
                GrabaLog(ConsoleColor.Red, "Uyyyyyy !!! Algo MUY GORDO ha pasado !!! ");
                GrabaLog(ConsoleColor.DarkRed, String.Concat(new object[] { ex.Message, ex.TargetSite }));
            }
        }

        void IniciaTareas()
        {
            try
            {
                //Parallel.ForEach(
                //       listaTareasCreadas, // source collection
                //       (itemtareacreada) =>
                //       {

                foreach (var itemtareacreada in listaTareasCreadas)
                {
                    if (itemtareacreada.Key.hhInicio <= timeClock)
                    {
                        if (!listaTareasActivas.ContainsKey(itemtareacreada.Value))
                        {
                            listaTareasActivas.GetOrAdd(itemtareacreada.Value, itemtareacreada.Key);
                            itemtareacreada.Value.Start();
                            listaTareasActivas[itemtareacreada.Value].IdTarea = itemtareacreada.Value.Id;
                        }
                    }
                    else
                    {
                        //for (int x = 0; x == 0; x++)
                        //{
                        //    for (int i = 0; i < listaTareasCreadas.Count; i++)
                        //    {
                        //GrabaLog(ConsoleColor.DarkGray,
                        //$"({itemtareacreada.Key.descripcion}) " +
                        //$"NO tiene que hacer «{itemtareacreada.Key.tarea.Method.Name.ToUpper()}» hasta las [{new TimeSpan(itemtareacreada.Key.hhInicio.Hour, itemtareacreada.Key.hhInicio.Minute, itemtareacreada.Key.hhInicio.Second)}], " +
                        //$"así que se va a rascar un poco los huevos mientras tanto");
                        //    }
                    }
                }
                //});

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        void FinalizaTareas()
        {
            int index = 0;
            object obj = new object();

            try
            {
                VerLogGestor();

                //Parallel.ForEach(
                //    listaTareasActivas,
                //    new ParallelOptions() { MaxDegreeOfParallelism = 2 },
                //    (itemactivas) =>
                //    {

                foreach (var itemactivas in listaTareasActivas)
                {
                    gestorInformation = null;

                    if (itemactivas.Key.IsCompleted)
                    {
                        TareaProgramada delTareaActiva;
                        if (!listaTareasActivas.TryRemove(itemactivas.Key, out delTareaActiva))
                        {
                            GrabaLog(ConsoleColor.Red, "Upsss !!! Algo ha pasado --> if (listaTareasActivas.TryRemove(item.Key, out delTareaActiva))");
                            throw new Exception();
                        }
                        else
                        {
                            Task delTareaCreada;
                            if (!listaTareasCreadas.TryRemove(delTareaActiva, out delTareaCreada))
                            {
                                GrabaLog(ConsoleColor.Red, "Upsss !!! Algo ha pasado --> (!listaTareasCreadas.TryRemove(delTareaActiva, out delTareaCreada))");
                                throw new Exception();
                            }
                            else
                            {
                                index = tareasprogramadas.IndexOf(delTareaActiva);
                                if (!tareasprogramadas[index].repetir)
                                {
                                    //                                    lock (obj)
                                    //                                  {
                                    tareasprogramadas.Remove(delTareaActiva);
                                    //                                }
                                }
                                else
                                {
                                    tareasprogramadas[index].IdTarea = 0;
                                    tareasprogramadas[index].hhFin = timeClock;
                                }
                                // Evento que notifica el fin de una tarea
                                gestorInformation = new GestorInfoEventArgs(delTareaActiva);
                                GestorTareaChange?.Invoke(this, gestorInformation);
                            }
                        }
                    }
                }//);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
            }
        }


        //Task<List<IGrouping<string, LogConsola>>> VerCande()
        //{
        //    try
        //    {
        //        return Task.FromResult((
        //            (from val in listaTareasCreadas
        //             select new LogConsola
        //             {
        //                 TareaPrg = val.Key,
        //                 IdTarea = val.Value.Id,
        //                 Tarea = val.Value
        //             }
        //            //).ToLookup(l => l.TareaPrg.descripcion
        //            ).ToLookup(l => l.TareaPrg.descripcion).ToList()).ToList());
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.ReadLine();
        //        throw new Exception(ex.Message);
        //    }

        //}

        Task<List<IGrouping<string, LogConsola>>> VerCande()
        {
            try
            {
                return Task.FromResult
                ((
                    (from val in listaTareasCreadas
                     select new LogConsola
                     {
                         TareaPrg = val.Key,
                         IdTarea = val.Value.Id,
                         Tarea = val.Value
                     }
                    ).ToLookup(l => l.TareaPrg.descripcion).ToList()).OrderBy(o => o.Key).ToList());
            }
            catch (Exception ex)
            {
                Console.ReadLine();
                throw new Exception(ex.Message);
            }

        }

        void VerLogGestor()
        {
            //Parallel.Invoke(() =>
            //{
            try
            {

                int ms = 0;
                TimeSpan span = new TimeSpan(0);
                string cadlog = string.Empty;

                //var cande =
                //         (
                //             (from val in listaTareasCreadas
                //              select new LogConsola
                //              {
                //                  TareaPrg = val.Key,
                //                  IdTarea = val.Value.Id,
                //                  Tarea = val.Value
                //              }
                //                 ).ToLookup(l => l.TareaPrg.descripcion
                //                 //).ToLookup(l => new { l.TareaPrg.descripcion }
                //                 ).ToList()
                //         ).ToList();

                var cande = Task.Run(() => VerCande());
                Task[] allTasks = { cande };
                Task.WaitAny(allTasks);

                //var pp = cande1.Result;

                //                Thread.Sleep(150);
                Console.Clear();
                GrabaLog(ConsoleColor.DarkGreen, "[Gestor]");
                GrabaLog(ConsoleColor.Gray, $"[{inicioGestor}] [{timeClock}] [{tiempoactividad}]");

                foreach (var item in cande.Result)
                //                    foreach (var item in cande.Result.OrderBy(o => o.Key))
                {
                    //GrabaLog(ConsoleColor.Black, Environment.NewLine);
                    //GrabaLog(ConsoleColor.Yellow, $"[{item.Key.descripcion}] ({item.Count()}){Environment.NewLine}");
                    GrabaLog(ConsoleColor.Yellow, $"{Environment.NewLine}[{item.Key}] ({item.Count()})");

                    //Parallel.ForEach(
                    //    item.OrderBy(o => o.TareaPrg.tarea.Method.Name),
                    //    new ParallelOptions() { MaxDegreeOfParallelism = 1 },
                    //    (item1) =>
                    //    {
                    foreach (var item1 in item)
                    //foreach (var item1 in item.OrderBy(o => o.TareaPrg.tarea.Method.Name))
                    {
                        switch (item1.Tarea.Status.ToString())
                        {
                            case "Created":
                                span = item1.TareaPrg.hhInicio - timeClock;
                                ms = (int)span.TotalMilliseconds / 1000;
                                //                            cadlog = $"[Creada=>{item1.TareaPrg.hhCreada} Id={item1.Tarea.Id} Status={item1.Tarea.Status} => ({ms})]=> [{item1.TareaPrg.tarea.Method.Name}=>[Inicio=>{item1.TareaPrg.hhInicio} Intervalo=> ({item1.TareaPrg.tsintervalo.Duration()})({item1.TareaPrg.tsintervalo.Minutes}:{item1.TareaPrg.tsintervalo.Seconds})]";
                                //                            cadlog = $"[Creada=>{item1.TareaPrg.hhCreada}] Id={item1.Tarea.Id} Status={item1.Tarea.Status} => ({ms})]=> [{item1.TareaPrg.tarea.Method.Name}=>[Inicio=>{item1.TareaPrg.hhInicio} Intervalo=> ({item1.TareaPrg.tsintervalo.Duration()})]";
                                cadlog = $"[{item1.TareaPrg.tarea.Method.Name}] [Id={item1.Tarea.Id} Status={item1.Tarea.Status}=>[{item1.TareaPrg.hhCreada}] Inicio=>[{item1.TareaPrg.hhInicio}] " +
                                    //                                     $"Intervalo=>[{item1.TareaPrg.tsintervalo.Days}:{item1.TareaPrg.tsintervalo.Hours}:{item1.TareaPrg.tsintervalo.Minutes}:{item1.TareaPrg.tsintervalo.Seconds}]({ms})] " +
                                    $"Intervalo=>[{item1.TareaPrg.tsintervalo.Duration()}]({ms})]";

                                GrabaLog(ConsoleColor.Gray, cadlog);
                                break;

                            case "Running":
                                span = timeClock - item1.TareaPrg.hhInicio;
                                ms = (int)span.TotalMilliseconds / 1000;
                                //                            cadlog = $"[Inicio=>{item1.TareaPrg.hhInicio}] Id={item1.Tarea.Id} Status={item1.Tarea.Status} => ({ms})]=> [{item1.TareaPrg.tarea.Method.Name}]";
                                cadlog = $"[{item1.TareaPrg.tarea.Method.Name}] [Id={item1.Tarea.Id} Status={item1.Tarea.Status}=>[{item1.TareaPrg.hhInicio}]({ms})]";
                                GrabaLog(ConsoleColor.DarkYellow, cadlog);
                                break;

                            case "RanToCompletion":
                                span = timeClock - item1.TareaPrg.hhInicio;
                                ms = (int)span.TotalMilliseconds / 1000;
                                //cadlog = $"[Fin=>{dt} Id={item1.Tarea.Id} Status={item1.Tarea.Status} => ({ms})]=> [{item1.TareaPrg.tarea.Method.Name}]";
                                //                            cadlog = $"[Fin=>{timeClock}] Id={item1.Tarea.Id} Status={item1.Tarea.Status} => ({ms})]=> [{item1.TareaPrg.tarea.Method.Name}]";
                                cadlog = $"[{ item1.TareaPrg.tarea.Method.Name}] [Id={item1.Tarea.Id} Status={item1.Tarea.Status}=>[{timeClock}] Inicio=>[{item1.TareaPrg.hhInicio}]({ms})]";
                                GrabaLog(ConsoleColor.DarkGreen, cadlog);
                                break;

                            //case "WaitingForActivation":
                            //    span = item1.TareaPrg.hhInicio - dt;
                            //    ms = (int)span.TotalMilliseconds / 1000;
                            //    break;

                            //case "WaitingForChildrenToComplete":
                            //    span = item1.TareaPrg.hhInicio - dt;
                            //    ms = (int)span.TotalMilliseconds / 1000;
                            //    break;

                            case "WaitingToRun":
                                span = timeClock - item1.TareaPrg.hhInicio;
                                ms = (int)span.TotalMilliseconds / 1000;
                                //cadlog = $"[Esperando=>{timeClock}] Id={item1.Tarea.Id} Status={item1.Tarea.Status} => ({ms})]=> [{item1.TareaPrg.tarea.Method.Name}=>[Inicio=>{item1.TareaPrg.hhInicio}]";
                                cadlog = $"[{ item1.TareaPrg.tarea.Method.Name}] [Id={item1.Tarea.Id} Status={item1.Tarea.Status}=>[{timeClock}({ms})] Inicio=>[{item1.TareaPrg.hhInicio}]";
                                GrabaLog(ConsoleColor.Magenta, cadlog);
                                break;

                            default: // otro estado
                                cadlog =
                                   //$"[{dt}] " +
                                   $"[{timeClock}] " +
                                   $"{{{item1.TareaPrg.tarea.Method.Name} " +
                                   $"Id={item1.Tarea.Id} " +
                                   $"Status={item1.Tarea.Status} " +
                                   $"Creada [{item1.TareaPrg.hhCreada}] " +
                                   $"Inicio [{item1.TareaPrg.hhInicio}] ";
                                //                      GrabaLog(ConsoleColor.Red, cadlog);
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.ReadLine();
                throw new Exception(ex.Message);
            }
        }

        void GrabaLog(ConsoleColor color, string cad)
        {
            log.WriteLog(color, cad);
        }

        //public void Subscribe(Clock theClock)
        //{
        //    theClock.SecondChanged += new Clock.SecondChangeHandler(TimeHasChanged);
        //}

        void TimeHasChanged(object theClock, TimeInfoEventArgs dt)
        {
            timeClock = new DateTime(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second, dt.millisecond);
            TimeSpan span = timeClock - inicioGestor;
            tiempoactividad = new TimeSpan(span.Days, span.Hours, span.Minutes, span.Seconds);
            //if(span.Minutes == 3)
            //    Console.ReadLine();

            CreaTareas();
        }
    }
}

