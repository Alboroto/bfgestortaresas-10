﻿using BFGestorTareas.Model;
using System;
using System.IO;
using System.Threading.Tasks;

namespace BFGestorTareas
{

    /*  Pautas de implementación del patrón de diseño Singleton en C #:        
    1 - declarar un constructor que debe ser privado y sin parámetros.
        Esto es necesario porque no se permite que la clase se instancia desde fuera de la clase.
        Solo crea instancias desde dentro de la clase.
    2 - La clase debe declararse como sellada, lo que garantizará que no se pueda heredar. 
        Esto será útil cuando se trata de la clase anidada. 
    3 - Crear una variable estática privada que contendrá una referencia a la única instancia creada de la clase, si la hay.
    4 - Crear una propiedad/método estático público que devolverá la única instancia creada de la clase Singleton.
    5 - Este método o propiedad primero verifica si una instancia de la clase singleton está disponible o no.Si la instancia de singleton está disponible, entonces devuelve esa instancia de singleton; de lo contrario, creará una instancia y luego la devolverá.
    */

    public sealed class LogGestor
    {
        public delegate void LogChangeHandler(object logTareas, LogInfoEventArgs logInformation);
        public LogChangeHandler LogChanged;

        private static int counter = 0;
        private StreamWriter write = null;

        private LogGestor()
        {
            //counter++;
            //Console.WriteLine("Counter Value Log " + counter.ToString());
            //            write = CreaLogFile(Ruta(), true);
        }

        private static readonly Lazy<LogGestor> Instancelock = new Lazy<LogGestor>(() => new LogGestor());

        public static LogGestor GetInstance
        {
            get
            {
                return Instancelock.Value;
            }
        }

        public void WriteLog(ConsoleColor color, string cad)
        {
            LogInfoEventArgs logInformation = new LogInfoEventArgs(color, cad);
            if (LogChanged != null)
            {
                LogChanged(this, logInformation);
            }
        }

        StreamWriter CreaLogFile(string logFilePath, bool append)
        {
            StreamWriter sw = new StreamWriter(logFilePath, append);
            sw.AutoFlush = true;
            return sw;
        }

        string Ruta()
        {
            //var path = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), string.Concat(new object[] { $"..\\..\\log\\{DateTime.Now.ToString("yyyy-MM-dd_HHmmss")}.log" }));
            var dire = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), string.Concat(new object[] { $"..\\..\\Log" }));
            if (!Directory.Exists(dire))
                Directory.CreateDirectory(dire);
            return string.Concat(new object[] { dire, $"\\LogGestor_{DateTime.Now.ToString("yyyy-MM-dd_HHmmss")}.log" });
        }
    }

    public class LogInfoEventArgs : EventArgs
    {
        public string logGestor;
        public ConsoleColor consoleColor;
        public LogInfoEventArgs(ConsoleColor consoleColor, string logGestor)
        {
            this.consoleColor = consoleColor;
            this.logGestor = logGestor;
        }
    }

    internal class LogConsola
    {
        internal Task Tarea;
        internal int IdTarea;
        internal TareaProgramada TareaPrg;

        //public string Descripcion;
        //public string Metodo;
        //public string IdTarea;
        //public string Estado;
        //public string HHCreada;
        //public string HHInicio;
        //public string HHUltima;
        //public string Intervalo;
    }
}

