﻿using BFGestorTareas.Model;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
/*
 * https://www.nimaara.com/practical-parallelization-with-map-reduce-in-c/ 
*/

/*  Pautas de implementación del patrón de diseño Singleton en C #:        
1 - declarar un constructor que debe ser privado y sin parámetros.
    Esto es necesario porque no se permite que la clase se instancie desde fuera de la clase.
    Solo crea instancias desde dentro de la clase.
2 - La clase debe declararse como sellada, lo que garantizará que no se pueda heredar. 
    Esto será útil cuando se trata de la clase anidada. 
3 - Crear una variable estática privada que contendrá una referencia a la única instancia creada de la clase, si la hay.
4 - Crear una propiedad/método estático público que devolverá la única instancia creada de la clase Singleton.
5 - Este método o propiedad primero verifica si una instancia de la clase singleton está disponible o no.
    Si la instancia de singleton está disponible, entonces devuelve esa instancia de singleton; de lo contrario, creará una instancia y luego la devolverá.
*/

namespace BFGestorTareas
{
    public delegate void GestorTareaChangeHandler(object sender, GestorInfoEventArgs e);

    public class GestorInfoEventArgs : EventArgs
    {
        public TareaProgramada tareaProgramada;
        public GestorInfoEventArgs(TareaProgramada tareaProgramada)
        {
            this.tareaProgramada = tareaProgramada;
        }
    }

    public sealed class GestorTareasSingleton
    {

        public GestorTareaChangeHandler GestorTareaChange;

        //        public event PropertyChangedEventHandler PropertyChanged;

        Clock theClock;
        LogGestor log;

        DateTime timeClock;
        DateTime inicioGestor;
        TimeSpan tiempoactividad;
        CancellationTokenSource cts;

        BlockingCollection<TareaProgramada> tareasprogramadas = null;
        ConcurrentDictionary<TareaProgramada, Task> listaTareasCreadas = null;
        ConcurrentDictionary<Task, TareaProgramada> listaTareasActivas = null;
        GestorInfoEventArgs gestorInformation = null;

        //private static int counter = 0;

        private GestorTareasSingleton()
        {

            //counter++;
            //Console.WriteLine("Counter Value GestorTareasSingleton " + counter.ToString());
            inicioGestor = DateTime.Now;
            tareasprogramadas = new BlockingCollection<TareaProgramada>();
            listaTareasCreadas = new ConcurrentDictionary<TareaProgramada, Task>();
            listaTareasActivas = new ConcurrentDictionary<Task, TareaProgramada>();
            log = LogGestor.GetInstance;
            theClock = Clock.GetInstance;
            theClock.SecondChanged += new Clock.SecondChangeHandler(TimeHasChanged);
        }

        //public GestorTareas()
        //{
        //    inicioGestor = DateTime.Now;
        //    tareasprogramadas = new List<TareaProgramada>();
        //    listaTareasCreadas = new ConcurrentDictionary<TareaProgramada, Task>();
        //    listaTareasActivas = new ConcurrentDictionary<Task, TareaProgramada>();
        //    log = LogGestor.GetInstance;
        //    theClock = Clock.GetInstance;
        //    theClock.SecondChanged += new Clock.SecondChangeHandler(TimeHasChanged);
        //}

        private static readonly Lazy<GestorTareasSingleton> Instancelock = new Lazy<GestorTareasSingleton>(() => new GestorTareasSingleton());

        public static GestorTareasSingleton GetInstance
        {
            get
            {
                return Instancelock.Value;
            }
        }

        public void AddTarea(TareaProgramada tarea)
        {
            string err = string.Empty;
            object locker = new object();

            try
            {
                //{

                // ****************************************************
                // 1 - Poner estas propiedades obligatorias en la clase;
                // 2 - O Añadirlas, no procesarlas y a avisar en el log
                // ****************************************

                if (tarea.tarea != null)
                {
                    if (tarea.descripcion != null)
                    {
                        if (tarea.tsintervalo.Ticks != 0 || !tarea.repetir)
                        {
                            if (tarea.tsintervalo.Ticks != 0 || tarea.repetir || tarea.forzarinicio)
                            {
                                tarea.hhInicio = DateTime.Now;
                                tarea.hhCreada = DateTime.MinValue;
                                tarea.hhFin = DateTime.MinValue;
                                tarea.IdTarea = 0;
                                if (!tareasprogramadas.Contains(tarea))
                                {
                                    tareasprogramadas.TryAdd(tarea);
                                }
                            }
                            else
                            {
                                GrabaLog(ConsoleColor.Red, $"[Tarea {tarea.tarea.Method.Name}]=> error => Falta intervalo de activación");
                            }
                        }
                        else
                        {
                            GrabaLog(ConsoleColor.Red, $"[Tarea {tarea.tarea.Method.Name}]=> error => Falta intervalo de repetición");
                        }
                    }
                    else
                    {
                        GrabaLog(ConsoleColor.Red, $"[Tarea {tarea.tarea.Method.Name}]=> error => No pertenece a nadie, asignar descripción");
                    }
                }
                else
                {
                    GrabaLog(ConsoleColor.Red, "error => La tarea no pude ser NULL ");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void RemoveTarea(TareaProgramada tarea)
        {
            try
            {
                //tareasprogramadas.Remove(tarea);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public void StartGestor()
        {
            theClock.StarClock();
        }

        public void StopGestor()
        {
            theClock.StopClock();
        }

        void Procesar()
        {
            CreaTareas();
            VerLogGestor();
            IniciaTareas();
            FinalizaTareas();
        }

        void CreaTareas()
        {
            //return Task.Run(() =>
            //{
            try
            {

                //Parallel.ForEach(
                //       tareasprogramadas,
                //        new ParallelOptions() { MaxDegreeOfParallelism = 2 },
                //       (itemtareas) =>
                //       {

                foreach (var itemtareas in tareasprogramadas)
                {
                    if (!listaTareasCreadas.ContainsKey(itemtareas))
                    {
                        itemtareas.hhCreada = timeClock;
                        itemtareas.hhInicio = itemtareas.forzarinicio ? itemtareas.hhCreada : timeClock.Add(itemtareas.tsintervalo);
                        //                        itemtareas.forzarinicio = false;
                        //listaTareasCreadas.GetOrAdd(itemtareas, new Task(new Action(itemtareas.tarea)));
                        //listaTareasCreadas.GetOrAdd(itemtareas, new Task(new Action(itemtareas.tarea), TaskCreationOptions.LongRunning));
                        var si = listaTareasCreadas.TryAdd(itemtareas, new Task(new Action(itemtareas.tarea), (TaskCreationOptions.PreferFairness | TaskCreationOptions.DenyChildAttach)));
                        //                        listaTareasCreadas.TryAdd(itemtareas, new Task(new Action(itemtareas.tarea), TaskCreationOptions.LongRunning));
                        //if (si)
                        //{
                        //    if (!itemtareas.repetir)
                        //    {
                        TareaProgramada tareaProgramada = itemtareas;
                        tareaProgramada = itemtareas;
                        tareasprogramadas.TryTake(out tareaProgramada);
                        //    }
                        //}
                    }
                }//);
                //Console.WriteLine($"Tareas programadas crear tareas => {tareasprogramadas.Count}");
            }
            catch (Exception ex)
            {
                GrabaLog(ConsoleColor.Red, "Uyyyyyy !!! Algo MUY GORDO ha pasado !!! ");
                GrabaLog(ConsoleColor.DarkRed, String.Concat(new object[] { ex.Message, ex.TargetSite }));
            }
            //Console.WriteLine($"Tareas Creadas => {listaTareasCreadas.Count}");
        }

        void IniciaTareas()
        {
            //return Task.Run(() =>
            //{
            //Parallel.ForEach(
            //       listaTareasCreadas, // source collection
            //        new ParallelOptions() { MaxDegreeOfParallelism = 2  },
            //       (itemtareacreada) =>
            //       {

            foreach (var itemtareacreada in listaTareasCreadas)
            {
                if (itemtareacreada.Key.hhInicio <= timeClock)
                {
                    //if (!listaTareasActivas.ContainsKey(itemtareacreada.Value))
                    //{
                    try
                    {
                        //listaTareasActivas.GetOrAdd(itemtareacreada.Value, itemtareacreada.Key);
                        //itemtareacreada.Value.Start();
                        //Task tareaCreada;
                        //listaTareasCreadas.TryRemove(itemtareacreada.Key, out tareaCreada);
                        //   listaTareasActivas[itemtareacreada.Value].IdTarea = itemtareacreada.Value.Id;
                        //if (!listaTareasActivas.ContainsKey(tareaCreada))
                        //    {
                        //        listaTareasActivas.GetOrAdd(tareaCreada, itemtareacreada.Key);
                        //        tareaCreada.Start();
                        //        //IniciaTareas();
                        //        //return;
                        //    }
                        //}

                        Task tareaCreada;
                        if (listaTareasCreadas.TryRemove(itemtareacreada.Key, out tareaCreada))
                        {
                            //listaTareasActivas.GetOrAdd(itemtareacreada.Value, itemtareacreada.Key);
                            //itemtareacreada.Value.Start();
                            //listaTareasActivas[itemtareacreada.Value].IdTarea = itemtareacreada.Value.Id;
                            if (!listaTareasActivas.ContainsKey(tareaCreada))
                            {
                                listaTareasActivas.GetOrAdd(tareaCreada, itemtareacreada.Key);
                                tareaCreada.Start();
                                //IniciaTareas();
                                //return;
                            }
                        }
                        //Console.WriteLine($"Tareas Activas => {listaTareasActivas.Count}");
                        //listaTareasActivas.GetOrAdd(tareaCreada, itemtareacreada.Key);                                
                    }
                    catch (Exception)
                    {
                        Console.WriteLine();
                    }
                    finally
                    {
                    }
                }
            }
        }

        void FinalizaTareas()
        {
            //Thread.Sleep(1000);

            //Parallel.ForEach(
            //    listaTareasActivas,
            //    new ParallelOptions() { MaxDegreeOfParallelism = 2 },
            //    (itemactivas) =>

            foreach (var itemactivas in listaTareasActivas)
            {
                if (itemactivas.Key.IsCompleted)
                {
                    TareaProgramada delTareaActiva = null;
                    try
                    {
                        if (!listaTareasActivas.TryRemove(itemactivas.Key, out delTareaActiva))
                        {
                            //Console.WriteLine($"Error Activas => {errActivas})");
                            //GrabaLog(ConsoleColor.Red, "Upsss !!! Algo ha pasado --> if (listaTareasActivas.TryRemove(item.Key, out delTareaActiva))");
                            //throw new Exception();
                        }
                        else if (delTareaActiva.repetir)
                        {

                            delTareaActiva.hhInicio = timeClock;
                            delTareaActiva.hhCreada = DateTime.MinValue;
                            delTareaActiva.hhFin = DateTime.MinValue;
                            delTareaActiva.IdTarea = 0;
                            delTareaActiva.forzarinicio = false;
                            //                            if (!tareasprogramadas.Contains(delTareaActiva))
                            //                          {
                            tareasprogramadas.TryAdd(delTareaActiva);
                            //                            }
                        }
                        gestorInformation = new GestorInfoEventArgs(delTareaActiva);
                        GestorTareaChange?.Invoke(this, gestorInformation);
                    }

                    //Console.WriteLine($"Tareas Activas => {listaTareasActivas.Count}");
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                    finally
                    {
                    }
                }
            }//);
        }

        Task<List<IGrouping<string, LogConsola>>> VerCande()
        {
            try
            {
                return Task.FromResult
                ((
                    (from val in listaTareasCreadas
                     select new LogConsola
                     {
                         TareaPrg = val.Key,
                         IdTarea = val.Value.Id,
                         Tarea = val.Value
                     }
                    ).ToLookup(l => l.TareaPrg.descripcion).ToList()).OrderBy(o => o.Key).ToList()); ;
            }
            catch (Exception ex)
            {
                Console.ReadLine();
                throw new Exception(ex.Message);
            }

        }

        void VerLogGestor()
        {
            //Parallel.Invoke(() =>
            //{
            try
            {
                int ms = 0;
                TimeSpan span = new TimeSpan(0);
                string cadlog = string.Empty;

                var act = listaTareasActivas.Select(c => new { c.Key, c.Value }).ToDictionary(k => k.Value, a => a.Key);
                var cre = listaTareasCreadas.Union(act).ToList();
                var cande = cre.Select(a => new LogConsola { TareaPrg = a.Key, IdTarea = a.Key.IdTarea, Tarea = a.Value }).ToLookup(l => l.TareaPrg.descripcion).OrderBy(o => o.Key).ToList();

                Console.Clear();
                GrabaLog(ConsoleColor.DarkGreen, "[Gestor Singleton]");
                GrabaLog(ConsoleColor.Gray, $"[{inicioGestor}] [{timeClock}] [{tiempoactividad}]");

                //Parallel.ForEach(
                //    cande.Result.OrderBy(o => o.Key),
                //    new ParallelOptions() { MaxDegreeOfParallelism = 1 },
                //    (item) =>
                //    {
                foreach (var item in cande)
                //                    foreach (var item in cande.Result.OrderBy(o => o.Key))
                {
                    GrabaLog(ConsoleColor.Yellow, $"{Environment.NewLine}[{item.Key}] ({item.Count()})");

                    //Parallel.ForEach(
                    //    item.OrderBy(o => o.TareaPrg.tarea.Method.Name),
                    //    new ParallelOptions() { MaxDegreeOfParallelism = 1 },
                    //    (item1) =>
                    //    {
                    foreach (var item1 in item)
                    //foreach (var item1 in item.OrderBy(o => o.TareaPrg.tarea.Method.Name))
                    {
                        switch (item1.Tarea.Status.ToString())
                        {
                            case "Created":
                                span = item1.TareaPrg.hhInicio - timeClock;
                                ms = (int)span.TotalMilliseconds / 1000;
                                //                            cadlog = $"[Creada=>{item1.TareaPrg.hhCreada} Id={item1.Tarea.Id} Status={item1.Tarea.Status} => ({ms})]=> [{item1.TareaPrg.tarea.Method.Name}=>[Inicio=>{item1.TareaPrg.hhInicio} Intervalo=> ({item1.TareaPrg.tsintervalo.Duration()})({item1.TareaPrg.tsintervalo.Minutes}:{item1.TareaPrg.tsintervalo.Seconds})]";
                                //                            cadlog = $"[Creada=>{item1.TareaPrg.hhCreada}] Id={item1.Tarea.Id} Status={item1.Tarea.Status} => ({ms})]=> [{item1.TareaPrg.tarea.Method.Name}=>[Inicio=>{item1.TareaPrg.hhInicio} Intervalo=> ({item1.TareaPrg.tsintervalo.Duration()})]";
                                cadlog = $"[{item1.TareaPrg.tarea.Method.Name}] [Id={item1.Tarea.Id} Status={item1.Tarea.Status}=>[{item1.TareaPrg.hhCreada}] Inicio=>[{item1.TareaPrg.hhInicio}] " +
                                    //                                     $"Intervalo=>[{item1.TareaPrg.tsintervalo.Days}:{item1.TareaPrg.tsintervalo.Hours}:{item1.TareaPrg.tsintervalo.Minutes}:{item1.TareaPrg.tsintervalo.Seconds}]({ms})] " +
                                    $"Intervalo=>[{item1.TareaPrg.tsintervalo.Duration()}]({ms})]";

                                GrabaLog(ConsoleColor.Gray, cadlog);
                                break;

                            case "Running":
                                span = timeClock - item1.TareaPrg.hhInicio;
                                ms = (int)span.TotalMilliseconds / 1000;
                                //                            cadlog = $"[Inicio=>{item1.TareaPrg.hhInicio}] Id={item1.Tarea.Id} Status={item1.Tarea.Status} => ({ms})]=> [{item1.TareaPrg.tarea.Method.Name}]";
                                cadlog = $"[{item1.TareaPrg.tarea.Method.Name}] [Id={item1.Tarea.Id} Status={item1.Tarea.Status}=>[{item1.TareaPrg.hhInicio}]({ms})]";
                                GrabaLog(ConsoleColor.DarkYellow, cadlog);
                                break;

                            case "RanToCompletion":
                                span = timeClock - item1.TareaPrg.hhInicio;
                                ms = (int)span.TotalMilliseconds / 1000;
                                //cadlog = $"[Fin=>{dt} Id={item1.Tarea.Id} Status={item1.Tarea.Status} => ({ms})]=> [{item1.TareaPrg.tarea.Method.Name}]";
                                //                            cadlog = $"[Fin=>{timeClock}] Id={item1.Tarea.Id} Status={item1.Tarea.Status} => ({ms})]=> [{item1.TareaPrg.tarea.Method.Name}]";
                                cadlog = $"[{ item1.TareaPrg.tarea.Method.Name}] [Id={item1.Tarea.Id} Status={item1.Tarea.Status}=>[{timeClock}] Inicio=>[{item1.TareaPrg.hhInicio}]({ms})]";
                                GrabaLog(ConsoleColor.DarkGreen, cadlog);
                                break;

                            //case "WaitingForActivation":
                            //    span = item1.TareaPrg.hhInicio - dt;
                            //    ms = (int)span.TotalMilliseconds / 1000;
                            //    break;

                            //case "WaitingForChildrenToComplete":
                            //    span = item1.TareaPrg.hhInicio - dt;
                            //    ms = (int)span.TotalMilliseconds / 1000;
                            //    break;

                            case "WaitingToRun":
                                span = timeClock - item1.TareaPrg.hhInicio;
                                ms = (int)span.TotalMilliseconds / 1000;
                                //cadlog = $"[Esperando=>{timeClock}] Id={item1.Tarea.Id} Status={item1.Tarea.Status} => ({ms})]=> [{item1.TareaPrg.tarea.Method.Name}=>[Inicio=>{item1.TareaPrg.hhInicio}]";
                                cadlog = $"[{ item1.TareaPrg.tarea.Method.Name}] [Id={item1.Tarea.Id} Status={item1.Tarea.Status}=>[{timeClock}({ms})] Inicio=>[{item1.TareaPrg.hhInicio}]";
                                GrabaLog(ConsoleColor.Magenta, cadlog);
                                break;

                            default: // otro estado
                                cadlog =
                                   //$"[{dt}] " +
                                   $"[{timeClock}] " +
                                   $"{{{item1.TareaPrg.tarea.Method.Name} " +
                                   $"Id={item1.Tarea.Id} " +
                                   $"Status={item1.Tarea.Status} " +
                                   $"Creada [{item1.TareaPrg.hhCreada}] " +
                                   $"Inicio [{item1.TareaPrg.hhInicio}] ";
                                //                      GrabaLog(ConsoleColor.Red, cadlog);
                                break;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Console.ReadLine();
                throw new Exception(ex.Message);
            }
        }

        /*
        void VerLogGestor()
        {
            //Parallel.Invoke(() =>
            //{
            try
            {

                int ms = 0;
                TimeSpan span = new TimeSpan(0);
                string cadlog = string.Empty;

                //var cande =
                //    (
                //    (from val in listaTareasCreadas
                //     select new LogConsola
                //     {
                //         TareaPrg = val.Key,
                //         IdTarea = val.Value.Id,
                //         Tarea = val.Value
                //     }
                //    //).ToLookup(l => l.TareaPrg.descripcion
                //    ).ToLookup(l => l.TareaPrg.descripcion).ToList()).OrderBy(o => o.Key).ToList();

                var cande = Task.Run(() => VerCande());
                Task[] allTasks = { cande };
                Task.WaitAny(allTasks);

                //var pp = cande1.Result;

                //                Thread.Sleep(150);
                Console.Clear();
                GrabaLog(ConsoleColor.DarkGreen, "[Gestor Singleton]");
                GrabaLog(ConsoleColor.Gray, $"[{inicioGestor}] [{timeClock}] [{tiempoactividad}]");

                //Parallel.ForEach(
                //    cande.Result.OrderBy(o => o.Key),
                //    new ParallelOptions() { MaxDegreeOfParallelism = 1 },
                //    (item) =>
                //    {
                foreach (var item in cande.Result)
                //                    foreach (var item in cande.Result.OrderBy(o => o.Key))
                {
                    //GrabaLog(ConsoleColor.Black, Environment.NewLine);
                    //GrabaLog(ConsoleColor.Yellow, $"[{item.Key.descripcion}] ({item.Count()}){Environment.NewLine}");
                    GrabaLog(ConsoleColor.Yellow, $"{Environment.NewLine}[{item.Key}] ({item.Count()})");

                    //Parallel.ForEach(
                    //    item.OrderBy(o => o.TareaPrg.tarea.Method.Name),
                    //    new ParallelOptions() { MaxDegreeOfParallelism = 1 },
                    //    (item1) =>
                    //    {
                    //foreach (var item1 in item)
                    foreach (var item1 in item.OrderBy(o => o.TareaPrg.tarea.Method.Name))
                    {
                        switch (item1.Tarea.Status.ToString())
                        {
                            case "Created":
                                span = item1.TareaPrg.hhInicio - timeClock;
                                ms = (int)span.TotalMilliseconds / 1000;
                                //                            cadlog = $"[Creada=>{item1.TareaPrg.hhCreada} Id={item1.Tarea.Id} Status={item1.Tarea.Status} => ({ms})]=> [{item1.TareaPrg.tarea.Method.Name}=>[Inicio=>{item1.TareaPrg.hhInicio} Intervalo=> ({item1.TareaPrg.tsintervalo.Duration()})({item1.TareaPrg.tsintervalo.Minutes}:{item1.TareaPrg.tsintervalo.Seconds})]";
                                //                            cadlog = $"[Creada=>{item1.TareaPrg.hhCreada}] Id={item1.Tarea.Id} Status={item1.Tarea.Status} => ({ms})]=> [{item1.TareaPrg.tarea.Method.Name}=>[Inicio=>{item1.TareaPrg.hhInicio} Intervalo=> ({item1.TareaPrg.tsintervalo.Duration()})]";
                                cadlog = $"[{item1.TareaPrg.tarea.Method.Name}] [Id={item1.Tarea.Id} Status={item1.Tarea.Status}=>[{item1.TareaPrg.hhCreada}] Inicio=>[{item1.TareaPrg.hhInicio}] " +
                                    //                                     $"Intervalo=>[{item1.TareaPrg.tsintervalo.Days}:{item1.TareaPrg.tsintervalo.Hours}:{item1.TareaPrg.tsintervalo.Minutes}:{item1.TareaPrg.tsintervalo.Seconds}]({ms})] " +
                                    $"Intervalo=>[{item1.TareaPrg.tsintervalo.Duration()}]({ms})]";

                                GrabaLog(ConsoleColor.Gray, cadlog);
                                break;

                            case "Running":
                                span = timeClock - item1.TareaPrg.hhInicio;
                                ms = (int)span.TotalMilliseconds / 1000;
                                //                            cadlog = $"[Inicio=>{item1.TareaPrg.hhInicio}] Id={item1.Tarea.Id} Status={item1.Tarea.Status} => ({ms})]=> [{item1.TareaPrg.tarea.Method.Name}]";
                                cadlog = $"[{item1.TareaPrg.tarea.Method.Name}] [Id={item1.Tarea.Id} Status={item1.Tarea.Status}=>[{item1.TareaPrg.hhInicio}]({ms})]";
                                GrabaLog(ConsoleColor.DarkYellow, cadlog);
                                break;

                            case "RanToCompletion":
                                span = timeClock - item1.TareaPrg.hhInicio;
                                ms = (int)span.TotalMilliseconds / 1000;
                                //cadlog = $"[Fin=>{dt} Id={item1.Tarea.Id} Status={item1.Tarea.Status} => ({ms})]=> [{item1.TareaPrg.tarea.Method.Name}]";
                                //                            cadlog = $"[Fin=>{timeClock}] Id={item1.Tarea.Id} Status={item1.Tarea.Status} => ({ms})]=> [{item1.TareaPrg.tarea.Method.Name}]";
                                cadlog = $"[{ item1.TareaPrg.tarea.Method.Name}] [Id={item1.Tarea.Id} Status={item1.Tarea.Status}=>[{timeClock}] Inicio=>[{item1.TareaPrg.hhInicio}]({ms})]";
                                GrabaLog(ConsoleColor.DarkGreen, cadlog);
                                break;

                            //case "WaitingForActivation":
                            //    span = item1.TareaPrg.hhInicio - dt;
                            //    ms = (int)span.TotalMilliseconds / 1000;
                            //    break;

                            //case "WaitingForChildrenToComplete":
                            //    span = item1.TareaPrg.hhInicio - dt;
                            //    ms = (int)span.TotalMilliseconds / 1000;
                            //    break;

                            case "WaitingToRun":
                                span = timeClock - item1.TareaPrg.hhInicio;
                                ms = (int)span.TotalMilliseconds / 1000;
                                //cadlog = $"[Esperando=>{timeClock}] Id={item1.Tarea.Id} Status={item1.Tarea.Status} => ({ms})]=> [{item1.TareaPrg.tarea.Method.Name}=>[Inicio=>{item1.TareaPrg.hhInicio}]";
                                cadlog = $"[{ item1.TareaPrg.tarea.Method.Name}] [Id={item1.Tarea.Id} Status={item1.Tarea.Status}=>[{timeClock}({ms})] Inicio=>[{item1.TareaPrg.hhInicio}]";
                                GrabaLog(ConsoleColor.Magenta, cadlog);
                                break;

                            default: // otro estado
                                cadlog =
                                   //$"[{dt}] " +
                                   $"[{timeClock}] " +
                                   $"{{{item1.TareaPrg.tarea.Method.Name} " +
                                   $"Id={item1.Tarea.Id} " +
                                   $"Status={item1.Tarea.Status} " +
                                   $"Creada [{item1.TareaPrg.hhCreada}] " +
                                   $"Inicio [{item1.TareaPrg.hhInicio}] ";
                                //                      GrabaLog(ConsoleColor.Red, cadlog);
                                break;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Console.ReadLine();
                throw new Exception(ex.Message);
            }
        }
        */
        void GrabaLog(ConsoleColor color, string cad)
        {
            //Console.ForegroundColor= color;
            //Console.WriteLine(cad);
            log.WriteLog(color, cad);
        }

        void Subscribe(Clock theClock)
        {
            theClock.SecondChanged += new Clock.SecondChangeHandler(TimeHasChanged);
        }

        void TimeHasChanged(object theClock, TimeInfoEventArgs dt)
        {
            timeClock = new DateTime(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second, dt.millisecond);
            TimeSpan span = timeClock - inicioGestor;
            tiempoactividad = new TimeSpan(span.Days, span.Hours, span.Minutes, span.Seconds);
            //if(span.Minutes == 3)
            //    Console.ReadLine();
            Procesar();
        }
    }
}
