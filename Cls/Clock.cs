﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace BFGestorTareas
{

    //     *  https://dotnettutorials.net/lesson/multicast-delegate-csharp/

    // Clase que pasaremos como 2º argumento del delegado y que recibiran los subcriptores 

    public class TimeInfoEventArgs : EventArgs
    {

        public int day;
        public int month;
        public int year;
        public int hour;
        public int minute;
        public int second;
        public int millisecond;

        public TimeInfoEventArgs(int year, int month, int day, int hour, int minute, int second, int millisecond)
        {
            this.year = year;
            this.month = month;
            this.day = day;
            this.hour = hour;
            this.minute = minute;
            this.second = second;
            this.millisecond = millisecond;
        }
    }

    public sealed class Clock
    {
        // Declaramos el delegado
        internal delegate void SecondChangeHandler(object clock, TimeInfoEventArgs timeInformation);
        // Lo instanciamos
        internal SecondChangeHandler SecondChanged;

        int day;
        int month;
        int year;
        int hour;
        int minute;
        int second;
        int millisecond;
        DateTime dt;
        int counter = 0;

        static bool _stopReloj;

        private Clock()
        {
            //counter++;
            //Console.WriteLine("Counter Value Clock " + counter.ToString());
            StopClock();
        }

        private static readonly Lazy<Clock> Instancelock = new Lazy<Clock>(() => new Clock());
        public static Clock GetInstance
        {
            get
            {
                return Instancelock.Value;
            }
        }

        //public Clock(int intervalo)
        //{
        //    _intervalo = intervalo;
        //    this.con = 0;
        //    StopClock();
        //}

        internal void StarClock()
        {
            if (!_stopReloj)
                return;

            _stopReloj = false;
            RunReloj();
        }

        internal void StopClock()
        {
            _stopReloj = true;
        }

        void RunReloj()
        {
            Parallel.ForEach(Infinite(), new ParallelOptions() { MaxDegreeOfParallelism = 1 }, new Action<bool>((val) =>
            //      Parallel.ForEach(Infinite(), new ParallelOptions(), new Action<bool>((val) =>
            { 
//            while (!_stopReloj)
            //{
                //                Console.WriteLine($"Reloj ThreadID{Thread.CurrentThread.ManagedThreadId}");

                Thread.Sleep(500);

                dt = System.DateTime.Now;
                //if (dt.Second != second)
                //{
                    // Creamos el ojete TimeInfoEventArgs para pasarselo a los ojetes que se subcriban a this.ojete
                    TimeInfoEventArgs timeInformation = new TimeInfoEventArgs(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second, dt.Millisecond);
                    // SI ALGUN OJETE SE HA SUBSCRITO LE CONTAMOS QUE HA CAMBIADO EL TIEMPO
                    if (SecondChanged != null)
                    {
                        SecondChanged(this, timeInformation);
                    }
                    else
                    {
                        Console.Clear();
                        Console.Write($"Esperando algún amigo: {new TimeSpan(dt.Hour, dt.Minute, dt.Second)}");
                        //                        Thread.Sleep(10 * 1000);
                    }
                //}

                //Console.ForegroundColor = ConsoleColor.DarkRed;
                day = dt.Day;
                month = dt.Month;
                year = dt.Year;
                second = dt.Second;
                minute = dt.Minute;
                hour = dt.Hour;

            }));
        }

        static IEnumerable<bool> Infinite()
        {
            while (!_stopReloj)
            {
                yield return !_stopReloj;
            }
        }
    }
}


