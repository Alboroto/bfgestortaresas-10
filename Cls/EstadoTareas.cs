﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace BFGestorTareas.Cls
{
    public class EstadoTareas
    {
        public static string SQL_GTBBDD_CadenaConexion { get; set; }

        public EstadoTareas()
        {
            SQL_GTBBDD_CadenaConexion = @"Server=192.168.0.102\sqlexpress2017;Database=GestorTareas;User Id=sa;password=1Qaz2wsx@;Trusted_Connection=False;MultipleActiveResultSets=true;";
        }

        public void AddEstadoTareas(List<TablaEstadoTareas> estados)
        {
            string connString = SQL_GTBBDD_CadenaConexion;

            //variables to store the query results            

            try
            {
                //sql connection object
                using (SqlConnection conn = new SqlConnection(connString))
                {

                    string query = @"delete EstadoTareas ";

                    foreach (var item in estados)
                    {
                        query = query + " " + string.Format("insert into EstadoTareas select {0},'{1}','{2}','{3}'", item.IdTarea, item.Estado, item.FechaEmpiezo, item.FechaFin == new DateTime() ? item.FechaEmpiezo : item.FechaFin);
                    }
                    SqlCommand cmd = new SqlCommand(query, conn);
                    conn.Open();
                    cmd.ExecuteNonQuery();


                    //close connection
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                //display error message
                Console.WriteLine("Exception: " + ex.Message);
            }


        }
        public List<TablaEstadoTareas> getEstados()
        {
            string connString = SQL_GTBBDD_CadenaConexion;
            List<TablaEstadoTareas> _tareas = new List<TablaEstadoTareas>();
            //variables to store the query results            

            try
            {
                //sql connection object
                using (SqlConnection conn = new SqlConnection(connString))
                {

                    string query = @" SELECT * from EstadoTareas  order by 1";
                    SqlCommand cmd = new SqlCommand(query, conn);
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            TablaEstadoTareas tet = new TablaEstadoTareas();
                            tet.IdTarea = dr.GetInt32(0);
                            tet.Estado = dr.GetString(1);
                            tet.FechaEmpiezo = dr.GetDateTime(2);

                            _tareas.Add(tet);





                        }
                    }
                    else
                    {
                        Console.WriteLine("No data found.");
                    }

                    //close data reader
                    dr.Close();

                    //close connection
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                //display error message
                Console.WriteLine("Exception: " + ex.Message);
            }

            return _tareas;
        }





    }

    public class TablaEstadoTareas
    {

        public int IdTarea { get; set; }
        public string Estado { get; set; }
        public DateTime FechaEmpiezo { get; set; }
        public DateTime FechaFin { get; set; }
        public TablaEstadoTareas()
        {

        }
    }
}
