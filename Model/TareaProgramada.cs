﻿using System;

namespace BFGestorTareas.Model

{
    public class TareaProgramada
    {
        public int IdTarea { get; set; }
        public TimeSpan tsintervalo { get; set; }
        public string descripcion { get; set; }
        public DateTime hhCreada { get; set; }
        public DateTime hhInicio { get; set; }
        public Action tarea { get; set; }
        //        public Action<Action> tareaconpar { get; set; }
        public DateTime hhFin { get; set; }
        public bool forzarinicio { get; set; }
        public bool repetir { get; set; }
        




        public TareaProgramada()
        {
            forzarinicio = true;
            repetir = true;
        }

        public TareaProgramada(TareaProgramada tareaprg)
        {
            this.descripcion = tareaprg.descripcion;
            this.tsintervalo = tareaprg.tsintervalo;
            this.descripcion = tareaprg.descripcion;
            this.hhCreada = tareaprg.hhCreada;
            this.hhInicio = tareaprg.hhInicio;
            this.tarea = tareaprg.tarea;
            this.hhInicio = tareaprg.hhFin;
            this.forzarinicio = tareaprg.forzarinicio;
            this.repetir = tareaprg.repetir;
        }

    }
}
